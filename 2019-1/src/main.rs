use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("1.txt")?;
    let reader = BufReader::new(f);

    let sum = reader.lines().fold(0, |count, x| {
	fn fuel(x: i32) -> i32 {
	    if x > 0 {
		x + fuel(x / 3 - 2)
	    } else {
		0
	    }
	};

	count + fuel(x.unwrap().parse::<i32>().unwrap() / 3 - 2)
    });

    println!("{}", sum);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn read_data() -> io::Result<()>{
	let f = File::open("1.txt")?;
	let mut reader = BufReader::new(f);
	let mut buffer = String::new();

	// read a line into buffer
	// reader.read_line(&mut buffer)?;

	let sum = reader.lines().fold(0, |count, x| {
	    fn fuel(x: i32) -> i32 {
		if x > 0 {
		    x + fuel(x / 3 - 2)
		} else {
		    0
		}
	    };

	    count + fuel(x.unwrap().parse::<i32>().unwrap() / 3 - 2)
	});
	println!("{}", sum);
	Ok(())
    }
}
