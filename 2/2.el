(setq IDs (with-temp-buffer
            (insert-file-contents "2.txt")
            (split-string (buffer-string)
                          "\n")))

(defun occurence (ch l)
  (cond
   ((null l) 0)
   ((equalp ch
            (first l))
    (+ 1
       (occurence ch
                  (rest l))))
   (t (occurence ch
                 (rest l)))))

(defun occurencies2 (line)
  (setq line-orig line)
  (occurencies3 line))

(defun occurencies3 (line)
  (cond
   ((null line) '())
   (t (setq occs (occurencies3 (rest line)))
      (cond
       ((not (assoc (first line) occs))
        (setq occs (cons (list (first line)
                               (occurence (first line)
                                          line-orig)) occs)))
       (t occs)))))

(defun is-count (count loccs)
  (cond
   ((null loccs) 0)
   ((equal count (second (first loccs))) 1)
   (t (is-count count
                (rest loccs)))))

(defun count-id (id)
  (cond
   ((equal id "") results)
   ((let ((occs (occurencies2 (split-string id "" t))))
      (setq results (list (+ (first results)
                             (is-count 2 occs))
                          (+ (second results)
                             (is-count 3 occs))))))))

(defun count (IDs)
  (setq results '(0 0))
  (county IDs)
  (* (first results)
     (second results)))

(defun county (IDs)
  (cond
   ((null IDs) results)
   (t (count-id (first IDs))
      (county (rest IDs)))))

(count IDs)

(defun split-str (str)
  (split-string str "" t)
  )

(defun num-diff-sym (w1 w2)
  (cond
   ((null w1) 0)
   (t (cond
       ((equal (first w1) (first w2)) (num-diff-sym (rest w1) (rest w2)))
       (t (+ 1 (num-diff-sym (rest w1) (rest w2))))

       ))
   )
  )

(defun find-similar-words (words)
  (cond
   ((null words) nil)
   ((null (rest words)) nil)
   (t
    (setq word (first words))
    (setq second-word (find-similar-word word (rest words)))
    (cond
     (second-word (list word second-word))
     (t (find-similar-words (rest words)))
     )
    )
   )
  )

(defun find-similar-word (word words)
  (cond
   ((null words) nil)
   ((equal 1 (num-diff-sym word (first words))) (first words))
   (t (find-similar-word word (rest words)))
   )
  )

(defun same-chars (w1 w2)
  (cond
   ((null w1) "")
   ((equal (first w1) (first w2)) (concat (first w1) (same-chars (rest w1) (rest w2))))
   (t (same-chars (rest w1) (rest w2)))
   )
  )

(apply `same-chars (find-similar-words (mapcar `split-str IDs)))
