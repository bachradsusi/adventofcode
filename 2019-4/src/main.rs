struct Passwords {
    value: [u32; 6],
    max: u32,
}

impl Passwords {
    fn new(mut min: u32, max: u32) -> Passwords {
        let mut passwords = Passwords {
            value: [0, 0, 0, 0, 0, 0],
            max: max,
        };
        let mut factor = 100000;
        for x in 0..6 {
            passwords.value[x] = min / factor;
            min = min - (passwords.value[x] * factor);
            factor = factor / 10;
        }
        passwords
    }

    fn get_value(&self) -> u32 {
        let mut u32_value = 0;
        let mut factor = 100000;
        for x in 0..6 {
            u32_value = u32_value + (self.value[x] * factor);
            factor = factor / 10;
        }
        u32_value
    }

    fn raise(&mut self, idx: usize) -> u32 {
        if self.value[idx] < 9 {
            self.value[idx] = self.value[idx] + 1;
        } else {
            self.value[idx] = self.raise(idx - 1);
        }
        self.value[idx]
    }

    fn match_criteria(&self) -> bool {
        let mut idx = 0;
        while idx < 5 {
            if self.value[idx] > self.value[idx + 1] {
                return false;
            }
            let mut double = 0;
            while idx < 5 && self.value[idx] == self.value[idx + 1] {
                double = double + 1;
                idx = idx + 1;
            }
            if double == 1 {
                return true;
            }
            idx = idx + 1
        }
        false
    }
}

impl Iterator for Passwords {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.raise(5);
            if self.get_value() > self.max {
                return None;
            }
            if self.match_criteria() {
                return Some(self.get_value());
            }
        }
    }
}

fn main() {
    let p = Passwords::new(234208, 765869);

    println!("{}", p.count());
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_raise() {
        let mut p = Passwords::new(123456, 987654);
        p.raise(5);
        println!("{} {}", p.get_value(), p.match_criteria());

        p = Passwords::new(123459, 987654);
        p.raise(5);
        println!("{} {}", p.get_value(), p.match_criteria());

        p = Passwords::new(123999, 987654);
        p.raise(5);
        println!("{} {}", p.get_value(), p.match_criteria());
    }

    #[test]
    fn test_criteria() {
        let mut p = Passwords::new(112233, 987654);
        println!("{} {}", p.get_value(), p.match_criteria());

        p = Passwords::new(123444, 987654);
        println!("{} {}", p.get_value(), p.match_criteria());

        p = Passwords::new(111122, 987654);
        println!("{} {}", p.get_value(), p.match_criteria());

        p = Passwords::new(111112, 987654);
        println!("{} {}", p.get_value(), p.match_criteria());
    }
}
