
(setq frequencies (mapcar 'string-to-number
                          (with-temp-buffer
                            (insert-file-contents "1.txt")
                            (split-string (buffer-string)
                                          "\n"
                                          t))))
(setq f frequencies)
(apply '+ frequencies)


(setq c 0)

(setq cs '())
(setq f frequencies)

(while (not (member c cs))
  (add-to-ordered-list 'cs c)
  ;; (push c cs)
  (setq c (+ c
             (pop f)))
  (if (not f)
      (setq f frequencies)))

(print c)


;; (add-to-ordered-list 'cs 4 2 )
