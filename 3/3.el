(defun read-file (filename)
  (with-temp-buffer
    (insert-file-contents filename)
    (split-string (buffer-string)
                  "\n" t)))

(defun gen-claim (line)
  (let (
        (id (apply 'concat (rest (split-string (first line) "" t))))
        (x (string-to-number (third line)))
        (y (string-to-number (second (split-string (third line) ","))))
        (sx (string-to-number (fourth line)))
        (sy (string-to-number (second (split-string (fourth line) "x"))))
        )
    (list id (list x y) (list (+ x sx) (+ y sy))
          )
    )
  )

(defun get-val (x y)
  (second (assoc y (second (assoc x fabric))))
  )

(defun pre-assoc (keynum fabline)
  (cond
   ((null fabline) '())
   ((equal keynum (first (first fabline))) '())
   (t (cons (first fabline) (pre-assoc keynum (rest fabline))))
   )
  )

(defun post-assoc (keynum fabline)
  (cond
   ((null fabline) '())
   ((equal keynum (first (first fabline))) (rest fabline))
   (t (post-assoc keynum (rest fabline)))
   )
  )

(defun set-val (x y val)
  (let ((fabline (second (assoc x fabric))))
    (cond
     ((not (assoc x fabric))
      (setq fabric (cons (list x (list (list y val))) fabric))
      )
     (t
      (setq fabric
            (append
             (pre-assoc x fabric)
             (cons
              (list x
                    (append
                     (pre-assoc y fabline)
                     (cons (list y val) (post-assoc y fabline))
                     )
                    )
              (post-assoc x fabric)
              )
             )
            )
      )
     )
    )
  )

(defun set-claim-line (x y my)
  (cond
   ((not (< y my)) '())
   (t
    (cond
     ((get-val x y) (set-val x y 2))
     (t (set-val x y 1)))
    (set-claim-line x (+ 1 y) my)
    )
   )
  )

(defun set-claim-x (x mx y my)
  (cond
   ((not (< x mx)) '())
   (t
    (set-claim-line x y my)
    (set-claim-x (+ 1 x) mx y my)
    )
   )
  )

;; (defun set-claim (claim)
;;   (let
;;       ((x (first (second claim)))
;;        (y (second (second claim)))
;;        (sx (first (third claim)))
;;        (sy (second (third claim)))
;;        )
;;     (set-claim-x x sx y sy)
;;     )
;;   )

(defun set-claim (claim)
  (set-claim-x (first (second claim))
       (second (second claim))
       (first (third claim))
       (second (third claim))
       )
  )


(defun count-line (line)
  (cond
   ((null line) 0)
   (t (cond
       ((equal (second (first line)) 2) (+ 1 (count-line (rest line))))
       (t (+ 0 (count-line (rest line))))
       )
      )
   )
  )

(defun count (fabric)
  (cond
   ((null fabric) 0)
   (t (+ (count-line (second (first fabric))) (count (rest fabric))))
   )
  )


(untrace-function 'set-claim-line)
(untrace-function 'set-claim-x)
(untrace-function 'set-claim)
(setq fabric '())
(setq lines (read-file "3.example.txt"))
(setq lines (read-file "3.txt"))
(setq claims (mapcar `gen-claim (mapcar (lambda (l) (split-string l " " t)) lines)))
(mapcar `set-claim claims)
fabric
(count fabric)


(setq fabric '(( 5 ( ( 6 1) ( 7 1 ) ( 8 1 ) ( 9 1 ))) ( 6 ( ( 6 1) ( 7 1 ) ( 8 1 ) ( 9 1 ))) ( 7 ( ( 6 1) ( 7 1 ) ( 8 1 ) ( 9 1 )))))
(setq fabline (second (assoc 4 fabric)))
(assoc 8 fabline)
(pre-assoc 5 fabric)
(post-assoc 8 fabric)
(append (pre-assoc 6 fabric) (cons (list 6 (second (assoc 6 fabric))) (post-assoc 6 fabric)))

(setq fabric '())
(set-val 2 3 1)
(set-val 2 4 2)
(set-val 2 5 3)
(setq g '((2 ((3 1) (5 3)))))
(setq h (second (assoc 2 fabric)))
(pre-assoc 4 h)
(post-assoc 4 h)
(list 2 (append (pre-assoc 4 h) (cons (list 4 7) (post-assoc 4 h))))
fabric
(set-val 3 3 1)
(set-val 3 4 2)
(set-val 3 5 1)


(gen)
(defun gen-new-fabric (x y f)
  (cond
   ((null f) '())
   ((cond
     ((equal x (first (first f))) (cons (gen-new-x (first f) y) (gen-new-fabric (rest f))))
     (t (cons (first f) (gen-new-fabric (rest f))))

   ))

   )
  )

(gen-new-fabric 3 3 fabric)

(defun set-val ( x y )
  (cond
   ;; ((null fabric) '())
   ((not (assoc x fabric))
    (setq fabric (cons (list x (list (list y 1))) fabric))


   )

   )
  )
(setq fabric '())
(set-val 2 2 )

(setq fabric '())
(defun gen-fabric (claims)
  (cond
   ((null claims) '())
   ((t (let (
             (x (first (second (first claims))))
             (y (second (second (first claims))))


        (cond
         (get-val )
         )


        )))
   )



  )
(get-val 9 5)
(setq l "#1300 @ 704,926: 5x4")
(setq line (split-string l " "))
(setq id (apply 'concat (rest (split-string (first line) "" t))))
(setq x (string-to-number (third line)))
(setq y (string-to-number (second (split-string (third line) ","))))
(setq sx (string-to-number (fourth line)))
(setq sy (string-to-number (second (split-string (fourth line) "x"))))

(setq fabric '(( 5 ( ( 6 1) ( 7 1 ) ( 8 1 ) ( 9 1 ))) ( 6 ( ( 6 1) ( 7 1 ) ( 8 1 ) ( 9 1 )))))
(assoc 6 (second (assoc 6 fabric)))
(numberp "3") (string-to-number "123,212" )


(split-string line " ")
(read-file "3.txt")
