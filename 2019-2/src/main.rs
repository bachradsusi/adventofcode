use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> io::Result<()> {
    let f = File::open("2.txt")?;
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    // read a line into buffer
    reader.read_line(&mut buffer)?;
    let mut input1 = 0;
    let mut input2 = 0;
    let noun_verb = loop {
	let mut instructions: Vec<i32> = buffer
            .split(',')
            .map(|x| x.trim().parse::<i32>().unwrap_or(0))
            .collect();
	instructions[1] = input1;
	instructions[2] = input2;
        let mut i = 0;
        while instructions[i] != 99 {
            let x = instructions[i + 1] as usize;
            let y = instructions[i + 2] as usize;
            let r = instructions[i + 3] as usize;
            instructions[r] = match instructions[i] {
                1 => instructions[x] + instructions[y],
                2 => instructions[x] * instructions[y],
                _ => -1,
            };
            i = i + 4;
        }
	if instructions[0] == 19690720 {
	    break (instructions[1], instructions[2])
	}
	if input2 as usize == instructions.len() - 1 {
	    input1 = input1 + 1;
	    input2 = 0;
	} else {
	    input2 = input2 + 1;
	}
    };
    println!("100 * {} + {} == {}", noun_verb.0 , noun_verb.1, 100 * noun_verb.0 + noun_verb.1);
    Ok(())
}
