use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> Result<(), io::Error> {
    let f = File::open("1.txt")?;
    let reader = BufReader::new(f);

    let mut numbers: Vec<u32> = reader
        .lines()
        .map(|x| x.unwrap().parse::<u32>().unwrap())
        .collect();
    while !numbers.is_empty() && numbers.len() > 2 {
        let a = numbers.remove(0);

        for i in 0..numbers.len() - 2 {
            for j in i + 1..numbers.len() - 1 {
                if a + numbers[i] + numbers[j] == 2020 {
                    println!("{}", a * numbers[i] * numbers[j])
                }
            }
        }
    }
    Ok(())
}
