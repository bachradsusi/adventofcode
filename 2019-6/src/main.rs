use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufReader};

struct Map {
    map: HashMap<String, Vec<String>>,
    reverse_map: HashMap<String, String>,
}

impl Map {
    fn new() -> Map {
        Map {
            map: HashMap::new(),
            reverse_map: HashMap::new(),
        }
    }

    fn add_object_to_orbit(&mut self, orbit: &str, object: &str) {
        if let Some(objects) = self.map.get_mut(&orbit.to_string()) {
            objects.push(object.to_string());
        } else {
            let objects = vec![object.to_string()];
            self.map.insert(orbit.to_string(), objects);
        };
        self.reverse_map
            .insert(object.to_string(), orbit.to_string());
    }

    fn count_orbits(&self, object: &str) -> usize {
        let mut co = 0;
        let mut pivot = object;
        while let Some(orbit) = self.reverse_map.get(&pivot.to_string()) {
            co = co + 1;
            pivot = orbit;
        }
        co
    }

    fn count(&self, orbit: &str) -> usize {
        let mut lc = 0;

        if let Some(objects) = self.map.get(&orbit.to_string()) {
            for object in objects {
                lc = lc + self.count_orbits(object) + self.count(object);
            }
            lc
        } else {
            0
        }
    }

    fn get_next(&self, from: &str) -> Vec<String> {
        let mut next_steps = Vec::new();
        if let Some(orbit) = self.reverse_map.get(&from.to_string()) {
            next_steps.push(orbit.to_string());
        }
        if let Some(objects) = self.map.get(&from.to_string()) {
            next_steps.extend(objects.iter().map(|x| x.to_string()));
        }
        next_steps
    }

    fn steps_to(&self, from: &str, to: &str) {}

    fn steps_from_to(&self, from: &str, to: &str) -> usize {
        let mut next_steps: Vec<String>;
        let mut steps = 0;
        let mut steps_q: VecDeque<String> = VecDeque::new();
        let mut steps_q_next: VecDeque<String> = VecDeque::new();
        let mut pivot = from.to_string();
        let mut visited: HashSet<String> = HashSet::new();

        next_steps = self.get_next(&pivot);

        loop {
            if next_steps.contains(&to.to_string()) {
                steps = steps + 1;
                break;
            }
            for step in next_steps {
                if visited.insert(step.to_string()) {
                    steps_q_next.push_back(step);
                }
            }
            if let Some(p) = steps_q.pop_front() {
                pivot = p;
            } else {
                steps = steps + 1;
                steps_q.append(&mut steps_q_next);
                pivot = steps_q.pop_front().unwrap();
            }
            next_steps = self.get_next(&pivot);
        }

        steps
    }
}

fn main() -> io::Result<()> {
    let filename = env::args().nth(1).unwrap();
    let f = File::open(filename)?;
    let reader = BufReader::new(f);
    let mut mapa = Map::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        let split: Vec<&str> = line.split(')').collect();
        mapa.add_object_to_orbit(split[0], split[1]);
    }

    println!("{}", mapa.count("COM"));
    println!("{}", mapa.steps_from_to("YOU", "SAN") - 2);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    use std::env;
    use std::fs::File;
    use std::io::prelude::*;
    use std::io::{self, BufReader};

    #[test]
    fn test_opcode() {
        let mut map = HashMap::new();
        let mut objects: Vec<String> = Vec::new();
        map.insert("COM".to_string(), objects);

        let mut orbit = map.get_mut("COM").unwrap();
        orbit.push("HELLO".to_string());

        println!("{:?}", &map);
    }

    #[test]
    fn test_get_next() -> io::Result<()> {
        let f = File::open("6.demo.txt")?;
        let reader = BufReader::new(f);
        let mut mapa = Map::new();

        for line in reader.lines().map(|l| l.unwrap()) {
            let split: Vec<&str> = line.split(')').collect();
            mapa.add_object_to_orbit(split[0], split[1]);
        }
        let mut next_step = mapa.get_next("COM");
        println!("{:?}", next_step);

        next_step = mapa.get_next("D");
        println!("{:?}", next_step);

        println!("{}", mapa.steps_from_to("K", "I"));

        Ok(())
    }
}
