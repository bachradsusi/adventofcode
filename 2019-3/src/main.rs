use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;

struct Paths {
    position: (i32, i32),
    paths: Vec<((i32, i32, i32), (i32, i32, i32))>,
}

impl Paths {
    fn move_push_horizontal(&mut self, value: i32) {
        let old_position = self.position;
        self.position.0 = self.position.0 + value;
        if value < 0 {
            self.paths.push((
                (self.position.0, old_position.0, -1),
                (self.position.1, self.position.1, 0),
            ));
        } else {
            self.paths.push((
                (old_position.0, self.position.0, 1),
                (self.position.1, self.position.1, 0),
            ));
        }
    }

    fn move_push_vertical(&mut self, value: i32) {
        let old_position = self.position;
        self.position.1 = self.position.1 + value;
        if value < 0 {
            self.paths.push((
                (self.position.0, self.position.0, 0),
                (self.position.1, old_position.1, -1),
            ));
        } else {
            self.paths.push((
                (self.position.0, self.position.0, 0),
                (old_position.1, self.position.1, 1),
            ));
        }
    }
}

fn main() -> io::Result<()> {
    let f = File::open("3.txt")?;
    let reader = BufReader::new(f);

    let mut both_paths: Vec<Paths> = Vec::new();

    for line in reader.lines().map(|x| x.unwrap()) {
        let mut paths = Paths {
            position: (0, 0),
            paths: Vec::new(),
        };

        for path in line.trim().split(',') {
            let (d, v) = path.trim().split_at(1);
            let v_i32 = v.parse::<i32>().unwrap();
            match d {
                "R" => paths.move_push_horizontal(v_i32),
                "L" => paths.move_push_horizontal(0 - v_i32),
                "U" => paths.move_push_vertical(v_i32),
                "D" => paths.move_push_vertical(0 - v_i32),
                _ => panic!("wtf"),
            }
        }
        both_paths.push(paths);
    }

    let mut crosses: HashMap<(i32, i32), i32> = HashMap::new();
    for h_path in both_paths[0].paths.iter() {
        both_paths[1].paths.iter().for_each(|v_path| {
            if (h_path.1).0 > (v_path.1).0
                && (h_path.1).1 < (v_path.1).1
                && (v_path.0).0 > (h_path.0).0
                && (v_path.0).1 < (h_path.0).1
            {
                crosses.insert(((v_path.0).0, (h_path.1).1), 0);
            }

            if (h_path.1).0 < (v_path.1).0
                && (h_path.1).1 > (v_path.1).1
                && (v_path.0).0 < (h_path.0).0
                && (v_path.0).1 > (h_path.0).1
            {
                crosses.insert(((v_path.1).0, (h_path.0).1), 0);
            }
        });
    }

    for one_path in both_paths.iter() {
        let mut steps = 0;
        let mut x;
        let mut x_end;
        let mut y;
        let mut y_end;
        let mut step;
        for path in one_path.paths.iter() {
            if (path.0).2 < 0 {
                x = (path.0).1;
                x_end = (path.0).0;
                step = -1;
            } else {
                x = (path.0).0;
                x_end = (path.0).1;
                step = 1;
            }
            y = (path.1).0;

            while x != x_end {
                x = x + step;
                steps = steps + 1;
                if let Some(path_steps) = crosses.get_mut(&(x, y)) {
                    let ns = *path_steps + steps;
                    crosses.insert((x, y), ns);
                }
            }
            if (path.1).2 < 0 {
                y = (path.1).1;
                y_end = (path.1).0;
                step = -1;
            } else {
                y = (path.1).0;
                y_end = (path.1).1;
                step = 1;
            }
            while y != y_end {
                y = y + step;
                steps = steps + 1;
                if let Some(path_steps) = crosses.get_mut(&(x, y)) {
                    let ns = *path_steps + steps;
                    crosses.insert((x, y), ns);
                }
            }
        }
    }

    let closest = crosses.iter().fold(0, |closest, cross| {
        if (cross.0).0.abs() + (cross.0).1.abs() < closest || closest < 1 {
            (cross.0).0.abs() + (cross.0).1.abs()
        } else {
            closest
        }
    });
    let shortest = crosses.iter().fold(0, |shortest, cross| {
        if shortest < 1 || cross.1 > &0 && cross.1 < &shortest {
            *cross.1
        } else {
            shortest
        }
    });
    println!("{} {}", closest, shortest);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn split_entry() {
        let s = String::from("R67");
        let (d, v) = s.split_at(1);
        println!("{} {}", d, v.parse::<i32>().unwrap_or(-1));
    }

    #[test]
    fn test_range_iter() {
        let x = 1;
        let y = 10;
        for a in (x..y).rev() {
            println!("{}", a);
        }
        let i = (x..y).rev();
    }
}
