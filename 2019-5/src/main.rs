use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::{self, Read};

struct Instructions {
    instructions: Vec<i32>,
    pointer: usize,
}

impl Instructions {
    fn new(buffer: &String) -> Instructions {
        let mut instructions = Instructions {
            instructions: Vec::new(),
            pointer: 0,
        };

        instructions.instructions = buffer
            .split(',')
            .map(|x| x.trim().parse::<i32>().unwrap_or(0))
            .collect();
        instructions
    }

    fn set_address(&mut self, address: usize, value: i32) {
        if address > self.instructions.len() {
            self.instructions.resize(address + 1, 0);
        }
        self.instructions[address] = value;
    }

    fn get_instruction(&self, instruction: &i32) -> (i32, i32, i32, i32) {
        let mut i = *instruction;
        let a = i / 10000;
        if a != 0 {
            i = i - a * 10000;
        }
        let b = i / 1000;
        if b != 0 {
            i = i - b * 1000;
        }
        let c = i / 100;
        if c != 0 {
            i = i - c * 100;
        }
        (a, b, c, i)
    }

    fn run_instructions(&mut self) -> io::Result<()> {
        loop {
            let instruction = self.get_instruction(&self.instructions[self.pointer]);
            if instruction.3 == 99 {
                break;
            }
            self.pointer = self.pointer + 1;

            if instruction.3 == 3 {
                let mut buffer = String::new();
                let stdin = io::stdin();
                let mut handle = stdin.lock();
                handle.read_to_string(&mut buffer)?;
                let address = self.instructions[self.pointer] as usize;
                self.set_address(address, buffer.trim().parse::<i32>().unwrap());
                self.pointer = self.pointer + 1;
            } else if instruction.3 == 4 {
                let mut address = self.pointer as usize;
                if instruction.2 == 0 {
                    address = self.instructions[address] as usize;
                }
                println!("{}", self.instructions[address]);
                self.pointer = self.pointer + 1;
            } else if instruction.3 == 5 {
                let mut y_addres = (self.pointer + 1) as usize;
                if instruction.1 == 0 {
                    y_addres = self.instructions[y_addres] as usize;
                };
                let mut x_addres = (self.pointer + 0) as usize;
                if instruction.2 == 0 {
                    x_addres = self.instructions[x_addres] as usize;
                };
                if self.instructions[x_addres] != 0 {
                    self.pointer = self.instructions[y_addres] as usize;
                } else {
                    self.pointer = self.pointer + 2;
                }
            } else if instruction.3 == 6 {
                let mut y_addres = (self.pointer + 1) as usize;
                if instruction.1 == 0 {
                    y_addres = self.instructions[y_addres] as usize;
                };
                let mut x_addres = (self.pointer + 0) as usize;
                if instruction.2 == 0 {
                    x_addres = self.instructions[x_addres] as usize;
                };
                if self.instructions[x_addres] == 0 {
                    self.pointer = self.instructions[y_addres] as usize;
                } else {
                    self.pointer = self.pointer + 2;
                }
            } else if instruction.3 == 7 {
                let mut r_addres = (self.pointer + 2) as usize;
                if instruction.0 == 0 {
                    r_addres = self.instructions[r_addres] as usize;
                };
                let mut y_addres = (self.pointer + 1) as usize;
                if instruction.1 == 0 {
                    y_addres = self.instructions[y_addres] as usize;
                };
                let mut x_addres = (self.pointer + 0) as usize;
                if instruction.2 == 0 {
                    x_addres = self.instructions[x_addres] as usize;
                };
                if self.instructions[x_addres] < self.instructions[y_addres] {
                    self.set_address(r_addres, 1);
                } else {
                    self.set_address(r_addres, 0);
                }
                self.pointer = self.pointer + 3;
            } else if instruction.3 == 8 {
                let mut r_addres = (self.pointer + 2) as usize;
                if instruction.0 == 0 {
                    r_addres = self.instructions[r_addres] as usize;
                };
                let mut y_addres = (self.pointer + 1) as usize;
                if instruction.1 == 0 {
                    y_addres = self.instructions[y_addres] as usize;
                };
                let mut x_addres = (self.pointer + 0) as usize;
                if instruction.2 == 0 {
                    x_addres = self.instructions[x_addres] as usize;
                };
                if self.instructions[x_addres] == self.instructions[y_addres] {
                    self.set_address(r_addres, 1);
                } else {
                    self.set_address(r_addres, 0);
                }
                self.pointer = self.pointer + 3;
            } else {
                let mut r_addres = (self.pointer + 2) as usize;
                if instruction.0 == 0 {
                    r_addres = self.instructions[r_addres] as usize;
                };
                let mut y_addres = (self.pointer + 1) as usize;
                if instruction.1 == 0 {
                    y_addres = self.instructions[y_addres] as usize;
                };
                let mut x_addres = (self.pointer + 0) as usize;
                if instruction.2 == 0 {
                    x_addres = self.instructions[x_addres] as usize;
                };
                let value = match instruction.3 {
                    1 => self.instructions[x_addres] + self.instructions[y_addres],
                    2 => self.instructions[x_addres] * self.instructions[y_addres],
                    _ => -1,
                };
                self.set_address(r_addres, value);
                self.pointer = self.pointer + 3;
            }
        }
        Ok(())
    }
}

fn main() -> io::Result<()> {
    let f = File::open("5.txt")?;
    let mut reader = BufReader::new(f);
    let mut buffer = String::new();
    // read a line into buffer
    reader.read_line(&mut buffer)?;

    let mut instructions = Instructions::new(&buffer);
    instructions.run_instructions().unwrap();
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_opcode() {
        let mut instruction = 10002;
        let mut oa = instruction / 10000;
        if oa != 0 {
            instruction = instruction - oa * 10000;
        }
        let mut ob = instruction / 1000;
        if ob != 0 {
            instruction = instruction - ob * 1000;
        }
        let mut oc = instruction / 100;
        if oc != 0 {
            instruction = instruction - oc * 100;
        }
        println!("{} {} {} {}", oa, ob, oc, instruction);
    }
}
